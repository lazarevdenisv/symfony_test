<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Users;

class UserController extends MainController
{
	public function login(Request $Request)
	{
		$User = new Users();

		$Form = $this->createFormBuilder($User)
			->add('email', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Log In'])
		->getForm();

		$Form->handleRequest($Request);

		if ($Form->isSubmitted())
		{
			$Data = $Request->request->get('form');
			$User = $this->getDoctrine()->getRepository('App:Users')->findOneByEmail($Data['email']);
			$this->LogUserIn($Request, $User->getName());

			return $this->redirect('/product/show');
		}

		return $this->render('users/login.html.twig', ['form' => $Form->createView()]);
	}

	public function logout(Request $Request)
	{
		$this->LogUserOut($Request);

		return $this->redirect('/product/show');
	}
}
?>