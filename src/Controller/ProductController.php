<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Products;
use Symfony\Component\Validator\Constraints\DateTime;

class ProductController extends MainController
{
	public function show(Request $Request)
	{
		$UserRole = $this->CheckUserRole($Request);

		$Products = $this->getDoctrine()->getRepository('App:Products')->findAll();

		return $this->render('products/show.html.twig', ['products' => $Products, 'user' => $UserRole]);
	}

	public function item(Request $Request, $id)
	{
		$UserRole = $this->CheckUserRole($Request);

		$Product = $this->getDoctrine()->getRepository('App:Products')->find($id);

		return $this->render('products/item.html.twig', ['product' => $Product, 'user' => $UserRole]);
	}

	public function create(Request $Request)
	{
		$UserRole = $this->CheckUserRole($Request);

		if ($UserRole['role'] == 'user')
		{
			$Product = new Products();

			$Form = $this->createFormBuilder($Product)
			->add('name', TextType::class)
			->add('category', TextType::class)
			->add('sku', TextType::class)
			->add('price', TextType::class)
			->add('quantity', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Save'])
			->getForm();

			$Form->handleRequest($Request);

			if ($Form->isSubmitted()) {
				$Data = $Request->request->get('form');
				$this->save($Data);

				return $this->redirect('/product/show');
			}

			return $this->render('products/add.html.twig', ['form' => $Form->createView()]);
		}
		else {throw $this->createNotFoundException('You are not allowed to create new product');}
	}

	public function edit(Request $Request, $id)
	{
		$UserRole = $this->CheckUserRole($Request);

		if ($UserRole['role'] == 'user')
		{
			$Manager = $this->getDoctrine()->getManager();
			$Product = $Manager->getRepository('App:Products')->find($id);

			if (!$Product) {throw $this->createNotFoundException('There are no product with the following id: '.$id);}

			$Form = $this->createFormBuilder($Product)
				->add('name', TextType::class)
				->add('category', TextType::class)
				->add('sku', TextType::class)
				->add('price', TextType::class)
				->add('quantity', TextType::class)
				->add('id', HiddenType::class, ['data' => $id])
				->add('save', SubmitType::class, ['label' => 'Update'])
			->getForm();

			$Form->handleRequest($Request);

			if ($Form->isSubmitted())
			{
				$Data = $Request->request->get('form');
				$this->save($Data);

				return $this->redirect('/product/show');
			}

			return $this->render('products/edit.html.twig', ['form' => $Form->createView(), 'product' => $Product]);
		}
		else {throw $this->createNotFoundException('You are not allowed to edit product');}
	}

	private function save($Data)
	{
		$Manager = $this->getDoctrine()->getManager();

		$Product =  (isset($Data['id'])) ? $Manager->getRepository('App:Products')->find($Data['id']) : new Products();

		$Product->setName($Data['name']);
		$Product->setCategory($Data['category']);
		$Product->setSku($Data['sku']);
		$Product->setPrice($Data['price']);
		$Product->setQuantity($Data['quantity']);

		if (!isset($Data['id']))
		{
			$Product->setCreatedAt(new \DateTime());
			$Product->setModifiedAt(new \DateTime());
			$Manager->persist($Product);
		}

		$Manager->flush();

		return true;
	}

	public function delete(Request $Request, $id)
	{
		$UserRole = $this->CheckUserRole($Request);

		if ($UserRole['role'] == 'user')
		{
			$Manager = $this->getDoctrine()->getManager();
			$Product = $Manager->getRepository('App:Products')->find($id);

			if (!$Product) {throw $this->createNotFoundException('There are no product with the following id: '.$id);}

			$Manager->remove($Product);
			$Manager->flush();

			return $this->redirect('/product/show');
		}
		else {throw $this->createNotFoundException('You are not allowed to delete product');}
	}
}
?>