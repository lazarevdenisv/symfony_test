<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class MainController extends AbstractController
{
	private function CheckSession(Request $Request)
	{
		$Session = $Request->getSession();

		if (!$Session->isStarted())
		{
			$Session = new Session();
			$Session->start();
		}

		return $Session;
	}

	protected function CheckUserRole(Request $Request)
	{
		$UserRole = [];
		$Session = $this->CheckSession($Request);

		if ($Session->has('user'))
		{
			$UserRole = [
				'role'	=> 'user',
				'name'	=> $Session->get('user'),
			];
		}
		else
		{
			$UserRole = [
				'role'	=> 'guest',
				'name'	=> '',
			];
		}

		return $UserRole;
	}

	protected function LogUserIn(Request $Request, $UserName)
	{
		$Session = $this->CheckSession($Request);
		$Session->set('user', $UserName);
	}

	protected function LogUserOut(Request $Request)
	{
		$Session = $this->CheckSession($Request);
		$Session->remove('user');
	}
}
?>