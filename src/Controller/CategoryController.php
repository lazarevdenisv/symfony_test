<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

use App\Entity\Categories;

class CategoryController extends MainController
{
	public function show(Request $Request)
	{
		$UserRole = $this->CheckUserRole($Request);

		$Categories = $this->getDoctrine()->getRepository('App:Categories')->findAll();

		return $this->render('categories/show.html.twig', ['categories' => $Categories, 'user' => $UserRole]);
	}
}
?>